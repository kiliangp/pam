# M06. ADMINISTRACIÓ AVANÇADA DE SISTEMES OPERATIUS

Pràctica d’autenticació: pam_ldap.so de l’escola
* Consulta el HowTo-PAM l’apartat “Implantació del servei nss-pam-ldap”
* Consulta el man de pam_ldap.so
* Crear la imatge: pam23:escola
Configura el container PAM (--privileged) per:
* Permetre l’autenticació local dels usuaris unix locals (unix01, unix02 i unix03)
* Permetre l’autenticació d’usuaris de LDAP de l’escola del treball.
* Als usuaris LDAP se’ls ha de crear el directori home automàticament si no existeix.
* Als usuaris se’ls munta automàticament dins el seu home un directori anomenat cloud corresponent al recurs WebDav de l’unitat de xarxa de l’escola de l’usuari.

Observacions de configuració:
* URI ldap.escoladeltreball.org
* BASE dc=escoladeltreball,dc=org
* WevDav https://cloud.proxy.inf.edt.cat:5511/remote.php/dav/files/<usuari>

## DOCKERFILE

Fitxer que utilitzarem per realitzar el "**docker build -t kiliangp/pam23:escola**". Dintre d'aquests tindrem la instal·lació de tots els paquets.

```
# ldapserver
FROM debian:latest
LABEL version="1.0"
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="M06 PAM23:escola"
RUN apt-get update && apt-get upgrade -y
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-ldapd libpam-pwquality libpam-mount nslcd nslcd-utils ldap-utils libnss-ldap davfs2
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```

## COMMON-SESSION

Dintre del fitxer estaràn tots els mòduls inclosos per l'inici de sessió dels usuaris.

```
#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of interactive sessions.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 

session optional	pam_mkhomedir.so

session	optional	pam_mount.so 
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
# end of pam-auth-update config
```
## DOCKER-COMPOSE.YML

Fitxer on podem engegar les dues imatges a la vegada amb "**docker compose up -d**".

```
version: "3"
services:
  pam:
    image: kiliangp/pam23:escola
    container_name: pam.edt.org
    hostname: pam.edt.org
    networks:
      - 2hisx
    privileged: trueUpdate pam_mount.conf.xml
networks:
  2hisx:
```
## LDAP.CONF

Indicarem a quina base de dades ens volem connectar automàticament, de manera que, si fem un **ldapsearch -x**, ens buscarà la base de dades de l'escola sense especificar paràmetres.

```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

BASE	dc=escoladeltreball,dc=org
URI	ldap://ldap.escoladeltreball.org

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt
```

## NSLCD.CONF

Aquest fitxer conté la configuració del servei nslcd, permetent l'administrador del sistema especificar com es deuen realitzar les consultes LDAP.  En el nostre cas, afegirem la base LDAP "escoladeltreball.org"

```
# /etc/nslcd.conf
# nslcd configuration file. See nslcd.conf(5)
# for details.

# The user and group nslcd should run as.
uid nslcd
gid nslcd

# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.escoladeltreball.org

# The search base that will be used for all queries.
base dc=escoladeltreball,dc=org

# The LDAP protocol version to use.
#ldap_version 3

# The DN to bind with for normal lookups.
#binddn cn=annonymous,dc=example,dc=net
#bindpw secret

# The DN used for password modifications by root.
#rootpwmoddn cn=admin,dc=example,dc=com

# SSL options
#ssl off
#tls_reqcert never
tls_cacertfile /etc/ssl/certs/ca-certificates.crt

# The search scope.
#scope sub
```

## NSSWITCH.CONF

La funció d'aquest arxiu és configurar la resolució de noms i la búsqueda d'informació en el sistema, en aquest cas, afegim ldap al costat de "files", això vol dir que primerament buscarà en arxius locals, en cas de que no trobi, buscarà per LDAP.

```
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         files ldap
group:          files ldap
shadow:         files
gshadow:        files

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis
```

## PAM_MOUNT.CONF.XML

Configurem el fitxer de muntatge automàtic dels sistemes d'arxiusl, de manera que ens concentrem amb el volum de webdav, el que ens farà que ens poguem connectar al nostre usuari i accedir al cloud.

```
<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE pam_mount SYSTEM "pam_mount.conf.xml.dtd">
<!--
	See pam_mount.conf(5) for a description.
-->

<pam_mount>

		<!-- debug should come before everything else,
		since this file is still processed in a single pass
		from top-to-bottom -->

<debug enable="0" />

		<!-- Volume definitions -->
    <volume 
        user="*"
        fstype="tmpfs"
        mountpoint="~/mytmp"
        options="size=100M"
        />

<volume 
      uid="*" 
      fstype="davfs" 
      path="https://cloud.proxy.inf.edt.cat:5511/remote.php/dav/files/%(USER)" 
      mountpoint="~/cloud"
      options="username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0770,dir_mode=0770"
/>

		<!-- pam_mount parameters: General tunables -->

<!--
<luserconf name=".pam_mount.conf.xml" />
-->

<!-- Note that commenting out mntoptions will give you the defaults.
     You will need to explicitly initialize it with the empty string
     to reset the defaults to nothing. -->
<mntoptions allow="nosuid,nodev,loop,encryption,fsck,nonempty,allow_root,allow_other" />
<!--
<mntoptions deny="suid,dev" />
<mntoptions allow="*" />
<mntoptions deny="*" />
-->
<mntoptions require="nosuid,nodev" />

<!-- requires ofl from hxtools to be present -->
<logout wait="0" hup="no" term="no" kill="no" />


		<!-- pam_mount parameters: Volume-related -->

<mkmountpoint enable="1" remove="true" />


</pam_mount>
```

## STARTUP.SH

S'executarà aquest script degut a que engeguem l'imatge en foreground.

```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml

/usr/sbin/nscd
/usr/sbin/nslcd
sleep infinity
#/bin/bash
```

## LDAPSEARCH -X | HEAD

Veiem si hi ha connectivitat amb la base de dades de l'escola, efectivament hi tenim.

```
root@pam:/opt/docker# ldapsearch -x | head 
# extended LDIF
#
# LDAPv3
# base <dc=escoladeltreball,dc=org> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# escoladeltreball.org
dn: dc=escoladeltreball,dc=org

```

## PRUEBAS WEBDAV

Per comprovar si es montar correctament el "cloud" del nostre usuari podem fer el següent. Observem que ens demana contrasenya, aquesta serà la que utilitzem al nostre compte. 

Veiem que ens surt un warning, aquest no li donarem importància ja que no afectarà al muntatge.

```
root@pam:/opt/docker# su - a221587kg -> ens connectem al nostre usuari.
Creating directory '/home/users/inf/hisx2/a221587kg'.
reenter password for pam_mount:
(mount.c:68): Messages from underlying mount program:
(mount.c:72): /sbin/mount.davfs: warning: the server does not support locks

a221587kg@pam:~$ ls -> llistem el que tenim dintre del directori principal, podem veure que hi ha un directori.
cloud  mytmp

a221587kg@pam:~/cloud$ ls -la -> llistem els arxius que es troben dintre del directori "a221587kgdav". Perfectament s'ha connectat amb el webdav del nostre usuari. Ens podem fixar que ens mostra l'hora que s'ha creat, el grup, i l'usuari.
total 19710
drwxrwx--- 7 a221587kg hisx2      496 Dec  5 10:39  .
drwxrwx--- 2 a221587kg hisx2        0 Nov 21 09:08  Documents
-rwxrwx--- 1 a221587kg hisx2 15187863 Nov 21 09:08 'Nextcloud Manual.pdf'
-rwxrwx--- 1 a221587kg hisx2  3963036 Nov 21 09:08 'Nextcloud intro.mp4'
-rwxrwx--- 1 a221587kg hisx2    50598 Nov 21 09:08  Nextcloud.png
drwxrwx--- 2 a221587kg hisx2        0 Nov 21 09:08  Photos
drwxrwx--- 7 a221587kg hisx2        0 Dec  5 10:39  Public
-rwxrwx--- 1 a221587kg hisx2      206 Nov 21 09:08  Readme.md
-rwxrwx--- 1 a221587kg hisx2   976625 Nov 21 09:08 'Reasons to use Nextcloud.pdf'
drwxrwx--- 2 a221587kg hisx2        0 Nov 21 09:08  Templates
-rwxrwx--- 1 a221587kg hisx2     2403 Nov 21 09:08 'Templates credits.md'
drwx------ 2 a221587kg hisx2        0 Jan 24 08:53  lost+found
```

## INTENTAR ACCEDIR DES DE UNIX01

Podem veure que la muntació ha fallat, degut a que l'usuari "unix" no està dintre de LDAP de l'escola. Per tant és normal que passi aquest error, per altre banda, no hi haurà problemes de creació de directori degu
```
root@pam:/opt/docker# su - unix01 
reenter password for pam_mount:
(mount.c:68): Messages from underlying mount program:
(mount.c:72): /sbin/mount.davfs: Mounting failed.
(mount.c:72): Could not authenticate to server: rejected Basic challenge
(pam_mount.c:522): mount of https://cloud.proxy.inf.edt.cat:5511/remote.php/dav/files/unix01 failed

unix01@pam:~$ pwd -> podem veure que el directori /home s'ha creat correctament gràcies al mòdul PAM "pam_mkhomedir.so"
/home/unix01
```
## GETENT PASSWD

Utilitzem l'ordre per consultar informació de les bases de dades del sistema que estan configurades al fitxer "nsswitch.conf", en el nostre cas "files  ldap".

```
a221587kg@pam:~$ getent passwd   
... (Més usuaris...)
a210993co:*:407766:400067:a210993co:/home/users/met/cefa1/a210993co:/bin/bash
a232905am:*:905507:900021:a232905am:/home/users/con/hcpe1/a232905am:/bin/bash
a181259et:*:407767:400021:a181259et:/home/users/met/hmmo2/a181259et:/bin/bash
a212238av:*:407768:400021:a212238av:/home/users/met/hmmo2/a212238av:/bin/bash

```

## GETENT GROUP 

Utilitzem aquesta ordre per saber detalls de tots els grups de la base de dades "escoladeltreball.org", incloent el nombre del grup  i una llista dels noms d'usuari que són membres d'aquell grup.

```
a221587kg@pam:~$ getent group
...(Més grups...)
abtx1:*:800054:a230037rf,a210428jc,a230032sg,a230023ch,a230047da,a230039ma,a230011sb,a230061jb,a231101bb,a230003nb,a231142mb,a230014nc,a230060mc,a230026ad,a230057gd,a230015pp,a230017ef,a230031ef,a230013ag,a230045jg,a220524ae,a220057lf,a220009ag,a230021ma,a230034aa,a231102na,a230010lm,a230030hm,a231105ac,a231107ac,a230025ac,a230008lc,a230019hc
cerc1:*:500053:a232776mp,a183487am,a211878al,a232774lk,a232775pp,a232773lp,a232778jh
```
