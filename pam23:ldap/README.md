# M06. ADMINISTRACIÓ AVANÇADA DE SISTEMES OPERATIUS
Configura el container PAM (--privileged) per:
*  Permetre l’autenticació local dels usuaris unix locals (unix01, unix02 i unix03)
* Permetre l’autenticació d’usuaris de LDAP.
Per fer-ho s’utilitzarà la imatge ldap23:latest (dap23:group) que conté el llistat final
d’usuaris i grups LDAP amb els que treballar.
* Als usuaris LDAP se’ls ha de crear el directori home automàticament si no existeix.
* Als usuaris LDAP se’ls ha de crear un recurs temporal, dins del home anomenat tmp.
Un ramdisk de tipus tmpfs de 100M.
* Tots els usuaris han de poder modificar-se el password. Els usuaris locals i també
els usuaris de LDAP.

## DOCKERFILE

Fitxer per crear la imatge kiliangp/pam23:ldap, en aquest fem la instal·lació de paquetst libnss-ldapd, libpam-ldapd nslcd...; tots aquests els utilitzarem per a la pràctica.
```
FROM debian:11
LABEL version="1.0"
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="PAM ldap"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get --fix-missing -y install procps iproute2 tree nmap vim less finger passwd  libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils libpam-mount libpam-pwquality
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```

## COMMON-SESSION

Modifiquem el fitxer "common-session", aquest especifica les configuracons de sessió comuns per els serveis que utilizin PAM.

En aquest li afegim el módul "pam_mkhomedir.so", per crear el directori /home els usuaris que es creïn, en el nostre cas, els usuaris "unix01,unix02...", per altre banda tenim el mòdul "pam_mount.so" que s'utilitza per montar automàticament sistemes d'arxius en el moment de l'inici de sessió d'un usuari i desmontar-los en el moment del seu tancament de sessió.

```
#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of interactive sessions.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session optional	pam_mkhomedir.so
session	optional	pam_mount.so
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000 
# end of pam-auth-update config
```

## DOCKER-COMPOSE.YML

Utilitzem un fitxer .yml per engegar els dues imatges en foreground, li afegim una nova opció a la imatge "pam" perquè s'encegui en --privileged.
A part el que fem és posar el hostname correctament perquè el LDAP agafi correctament l'uri.

```
version: "3"
services:
  ldap:
    image: kiliangp/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - 2hisx
  pam:
    image: kiliangp/pam23:ldap
    container_name: pam.edt.org
    hostname: pam.edt.org
    networks:
      - 2hisx
    privileged: true
networks:
 2hisx:
```

## LDAP.CONF 

Fitxer utilitzat per permetre la conexió predeterminada a una base de dades sense la utilització de la IP del container.

```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

BASE	dc=edt,dc=org
URI	ldap://ldap.edt.org

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt
```

## NSLCD.CONF 

La funció principal és permetre que els sistemes obtinguin informació d'usuaris, grups i altres objectes del servidor LDAP.

En aquest cas, especifiquem la base de dades a la qual ens volem connectar, i el seu URI.

```
# /etc/nslcd.conf
# nslcd configuration file. See nslcd.conf(5)
# for details.

# The user and group nslcd should run as.
uid nslcd
gid nslcd

# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.edt.org

# The search base that will be used for all queries.
base dc=edt,dc=org

# The LDAP protocol version to use.
#ldap_version 3

# The DN to bind with for normal lookups.
#binddn cn=annonymous,dc=example,dc=net
#bindpw secret

# The DN used for password modifications by root.
#rootpwmoddn cn=admin,dc=example,dc=com

# SSL options
#ssl off
#tls_reqcert never
tls_cacertfile /etc/ssl/certs/ca-certificates.crt

# The search scope.
#scope sub
```

## NSSWITCH.CONF

Aquest fitxer s'utilitza per configurar la resolució de noms i serveis, es defineix com el sistema operatiu ha de buscar informació sobre usuaris, grups, hosts i altres serveis.

```
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         files ldap
group:          files ldap
shadow:         files sss
gshadow:        files

hosts:          files mdns4_minimal [NOTFOUND=return] dns myhostname mymachines
networks:       files

protocols:      db files
services:       db files sss
ethers:         db files
rpc:            db files

netgroup:       nis sss
automount:      sss
```

## PAM_MOUNT.CONF.XML

S'utilitza per el servei "pam_mount", aquest mòdul permet montar i desmontar automàticament sistemes d'arxius en el moment de l'inici i tancament de sessió d'un usuari.

```
<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE pam_mount SYSTEM "pam_mount.conf.xml.dtd">
<!--
	See pam_mount.conf(5) for a description.
-->

<pam_mount>

		<!-- debug should come before everything else,
		since this file is still processed in a single pass
		from top-to-bottom -->

<debug enable="0" />

		<!-- Volume definitions -->

<volume
	user="*"
	fstype="tmpfs"
	mountpoint="~/tmp"
	options="size=100M"
/>

		<!-- pam_mount parameters: General tunables -->

<!--
<luserconf name=".pam_mount.conf.xml" />
-->

<!-- Note that commenting out mntoptions will give you the defaults.
     You will need to explicitly initialize it with the empty string
     to reset the defaults to nothing. -->
<mntoptions allow="nosuid,nodev,loop,encryption,fsck,nonempty,allow_root,allow_other" />
<!--
<mntoptions deny="suid,dev" />
<mntoptions allow="*" />
<mntoptions deny="*" />
-->
<mntoptions require="nosuid,nodev" />

<!-- requires ofl from hxtools to be present -->
<logout wait="0" hup="no" term="no" kill="no" />


		<!-- pam_mount parameters: Volume-related -->

<mkmountpoint enable="1" remove="true" />


</pam_mount>
```

## STARTUP.SH

Fitxer que s'executarà degut a que encenem el container en foreground, en aquest, passem els fitxers del nostre directori dintre del container, encenem dimonis, etc...

```
#!/bin/bash

for user in unix01 unix02 unix03
do
	useradd -m -s /bin/bash $user
	echo -e "$user\n$user" | passwd $user
done

cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf

/usr/sbin/nscd
/usr/sbin/nslcd
sleep infinity
```
## COMPROVACIONS

**ldapsearch -x** -> ordre per veure si hi ha connectivitat amb la base de dades "edt.org" del container "ldap.edt.org". Efectivament, agafa les dades des de el container "pam.edt.org".

```
...
# sudo, grups, edt.org
dn: cn=sudo,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: master
cn: sudo
gidNumber: 27
description: Usuaris que estan en el grup sudoers
memberUid: admin

# 1wiam, grups, edt.org
dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: 1wiam
gidNumber: 612
description: Clase d'ASIX que esta a la clase 1wiam

# 2wiam, grups, edt.org
dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: 2wiam
gidNumber: 613
description: Clase d'ASIX que esta a la clase 2wiam

# 1hiaw, grups, edt.org
dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Clase d'ASIX que esta a la clase 1hiaw

# search result
search: 2
result: 0 Success
# numResponses: 31
# numEntries: 30
```

**getent passwd marta** -> el sistema intentarà buscar informació sobre l'usuari marta en les fonts de dades configurades, que poden incloure arxius locals i el servidor LDAP.

```
marta:*:5003:600:Marta Mas:/tmp/home/marta:
```
**getent group** -> podem veure que ens detecta tots els grups de la base de dades ldap.edt.org que està en el servidor LDAP

```
root:x:0:
daemon:x:1:
bin:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mail:x:8:
news:x:9:
uucp:x:10:
man:x:12:
proxy:x:13:
kmem:x:15:
dialout:x:20:
fax:x:21:
voice:x:22:
cdrom:x:24:
floppy:x:25:
tape:x:26:
sudo:x:27:
audio:x:29:
dip:x:30:
www-data:x:33:
backup:x:34:
operator:x:37:
list:x:38:
irc:x:39:
src:x:40:
gnats:x:41:
shadow:x:42:
utmp:x:43:
video:x:44:
sasl:x:45:
plugdev:x:46:
staff:x:50:
games:x:60:
users:x:100:
nogroup:x:65534:
messagebus:x:101:
nslcd:x:102:
unix01:x:1000:
unix02:x:1001:
unix03:x:1002:
professors:*:601:anna,pere
alumnes:*:600:jordi,marta,pau
1asix:*:610:user04,user01,user02,user03
2asix:*:611:user10,user06,user07,user08,user09
master:*:27:admin
sudo:*:27:admin
1wiam:*:612:
2wiam:*:613:
1hiaw:*:614:
```
**su - unix01** -> ordre per veure si crea correctament el directori /home

```
root@pam:/opt/docker# su - unix01
reenter password for pam_mount:
unix01@pam:~$ pwd 
/home/unix01
```

**su - marta** -> la mateixa ordre però desde un usuari que es troba en el servidor LDAP. Podem veure que la creació del volum resulta correcta.

```
root@pam:/opt/docker# su - marta 
Creating directory '/tmp/home/marta'.
reenter password for pam_mount:
$ ls -> podem observar que ha llistat el volum creat amb el fitxer "pam_mount.conf.xml"
tmp

```

**PER ENCENDRE ELS CONTAINERS FAREM LA SEGÜENT ORDRE TENINT EL FITXER "docker-compose.yml" DESCARREGAT**
* **docker compose -f docker-compose.yml up -d**
