import pam

pam_service = pam.pam()

user_pam = input("Introdueix el nom de l'usuari: ")
passwd_pam = input("Introdueix el password: ")

pam_service.authenticate(user_pam, passwd_pam)

if pam_service.code == 0:
    for numeros in range(1,11):
        print(numeros)

else:
    print("Usuari no autenticat")